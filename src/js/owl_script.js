$(document).ready(function(){
	$(".owl-carousel").owlCarousel({
		loop:true,
		margin: 10,
		responsive:{
			0:{
				items:1
			},
			700:{
				items:2
			},
			1120:{
				items:3
			}
		}
	});
});